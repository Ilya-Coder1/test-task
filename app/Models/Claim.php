<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    protected $primaryKey = 'claim_id';

    protected $fillable = [
        'name',
        'email',
        'text'
    ];
}
