@extends('layout')

@section('content')
<div>
    <div class="mt-3">
        <h1>Сохранение заявки</h1>
        <form class="mt-3" method="POST" action="{{ route('saveClaim') }}">
            @csrf
            <div class="mb-3">
                <label class="form-label">Имя</label>
                <input type="text" class="form-control" name="name" />
            </div>
            <div class="mb-3">
                <label class="form-label">Email</label>
                <input type="email" class="form-control" name="email" />
            </div>
            <div class="mb-3">
                <label class="form-label">Текст заявки</label>
                <textarea class="form-control" style="resize: none" rows="10" name="text"> </textarea>
            </div>
            <div class="mb-3">
                <button class="btn btn-primary" type="submit">Сохранить</button>
            </div>
        </form>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>

    <div class="mt-3">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Имя</th>
                    <th scope="col">Email</th>
                    <th scope="col">Текст</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($claims as $claim)
                    <tr>
                        <td>{{ $claim->id }}</td>
                        <td>{{ $claim->name }}</td>
                        <td>{{ $claim->email }}</td>
                        <td>{{ $claim->text }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection