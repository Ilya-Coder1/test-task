<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    @vite(['resources/js/app.js'])
    <title>Test</title>
</head>
<body>
    <div class="container d-flex justify-content-center">
        @yield('content')
    </div>
</body>
</html>