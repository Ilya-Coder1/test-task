<?php

namespace App\Http\Controllers;

use App\Models\Claim;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('home.index', ['claims' => Claim::all()]);
    }

    public function saveClaim(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'text' => 'required',
        ]);

        $claim = Claim::create($request->all());

        return redirect(route('home'));
    }
}
